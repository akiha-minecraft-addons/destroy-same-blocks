// ユーザーが変更可能な設定
const settings = {
  // 破壊対象ブロック
  allowedBlockList: [
    'minecraft:log',
    'minecraft:leaves',
  ],
  // 破壊対象範囲
  // 無制限に取得し続けることを避ける
  destructionRange: {
    x: 10,
    y: 30,
    z: 10,
  },
  // 対象にどのようなコマンドを実行するか
  // 基本は「空気ブロックを破壊モードで置く」がそれ以外もできる
  setblockCommand: "air 0 destroy",
  // setblockCommand: "tnt",
}

/* global server */
const system = server.registerSystem(0, 0);

// 探索範囲
// 対象ブロックの前後左右上のみを対象にする
// 要するに下を除く隣接しているブロックのみ
const searchPositionList = [
  ['x', +1], ['x', -1],
  ['y', +1], // 下方向だけは取得しない
  ['z', +1], ['z', -1],
];

// 関数をまたいで保持しつつける変数
let tickingArea = null;
let basicPosition = null;
const targetBlockList = [];

// 周囲のブロック座標を取得する
function findAroundBlockPositions(blockPosition) {
  const positions = [];
  // 探索範囲で繰り返し
  searchPositionList.forEach((val) => {
    // 方向を配列から取り出してわかりやすい変数名にする
    const angle = val[0];
    // 対象ブロックをディープコピー（常に元の座標から数えたいので）
    const position = JSON.parse(JSON.stringify(blockPosition));
    // 座標を足して、目的の方向の座標にする
    position[angle] += val[1];
    // 対象座標と基準座標の距離（絶対値(absolute value)で取得して負の距離にならないようにする）
    const distance = Math.abs(basicPosition[angle] - position[angle]);
    // その距離は対象範囲内であるか（無制限に取得し続けることを避ける）
    if (distance <= settings.destructionRange[angle]) {
      // 範囲内のときだけ、その座標を破壊対象として配列に加える
      positions.push(position);
    }
  });
  return positions;
}

// 破壊対象ブロックであるかを判定する
function isAllowedBlock(block) {
  // 対象ブロックの種類が破壊対象ブロックに含まれているか
  return settings.allowedBlockList.some((allowed) => (allowed === block.__identifier__));
  // 処理内容は次と同じ
  /*
  for (const allowed of settings.allowedBlockList) {
    if (allowed === block.__identifier__) {
      return true;
    }
  }
  return false;
  */
}

// そのブロックはすでに破壊対象に含まれているか
function isListedBlock(block) {
  return targetBlockList.some(
    (listedBlock) => (
      listedBlock.block_position.x === block.block_position.x
      && listedBlock.block_position.y === block.block_position.y
      && listedBlock.block_position.z === block.block_position.z
    ),
  );
}

// 破壊対象のブロックを探す
function searchTargetBlock(block) {
  // 周囲6方向のブロックを調べる
  const positions = findAroundBlockPositions(block.block_position);
  positions.forEach((position) => {
    // ティック領域と座標からブロックを取得
    const b = system.getBlock(tickingArea, position);
    // 同じ種類のブロック判定を変数に入れる（下の判定で何をやっているかわかりやすくしている）
    const isSameTypeBlock = b.__identifier__ === block.__identifier__;
    // そのブロックが同じ種類のブロックであるか、許可されているか、まだ一覧に加えられていないか
    if (isSameTypeBlock && isAllowedBlock(b) && !isListedBlock(b)) {
      // 条件を満たせば対象に追加
      targetBlockList.push(b);
    }
  });
}

// ブロックをスラッシュコマンドで壊す
function runMyCommand(block) {
  // ブロックの座標を短い変数名に取り出す
  const p = block.block_position;
  // 座標をコマンド用の文字列に変換
  const position = `${p.x} ${p.y} ${p.z}`;
  // 文字列を組み合わせてコマンドを実行する
  system.executeCommand(`/setblock ${position} ${settings.setblockCommand}`);
}

// ブロックを破壊する
function destroyBlock(block) {
  // そのブロックに対して設定したコマンドを実行する
  runMyCommand(block)
  // 破壊したブロックを基準に再びブロックを探索する
  searchTargetBlock(block);
}

// プレイヤー基準でティック領域を取得する
system.getTickingArea = function getTickingArea(player) {
  const tickWorldComponent = this.getComponent(player, 'minecraft:tick_world');
  return tickWorldComponent.data.ticking_area;
};

// 最初に実行されるメソッド
system.initialize = function initialize() {
  this.listenForEvent(
    'minecraft:player_destroyed_block',
    (d) => this.onPlayerDestroyedBlock(d),
  );
};

// 毎ティック実行されるメソッド
system.update = function update() {
  // 1ティックに20回まで実行
  for (let i = 0; i < 10; i += 1) {
    // 配列に値がなければ繰り返し終了
    if (!targetBlockList.length) break;
    // 配列からブロックを取り出しブロック破壊関数に渡す
    destroyBlock(targetBlockList.pop());
  }
};

// system.displayChatEvent = function displayChatEvent(message) {
//   const BroadcastEventData = this.createEventData('minecraft:display_chat_event');
//   BroadcastEventData.data.message = message;
//   this.broadcastEvent('minecraft:display_chat_event', BroadcastEventData);
// };

// コマンドを実行する
system.executeCommand = function executeCommand(command) {
  const ExecuteEventData = this.createEventData('minecraft:execute_command');
  ExecuteEventData.data.command = command;
  this.broadcastEvent('minecraft:execute_command', ExecuteEventData);
};

// プレイヤーがブロックを壊したとき
system.onPlayerDestroyedBlock = function onPlayerDestroyedBlock(d) {
  // ティック領域を取得する（ティック領域のブロックしか取得できない）
  tickingArea = this.getTickingArea(d.data.player);
  // 最初の座標をグローバル変数に保持しておく
  basicPosition = d.data.block_position;
  // ブロックと同じ形式のオブジェクトで必要な情報を渡す
  destroyBlock({
    block_position: d.data.block_position,
    __identifier__: d.data.block_identifier, // 破壊対象の種類。これと同じブロックだけ壊す
  });
};
